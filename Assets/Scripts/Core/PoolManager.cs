﻿using UnityEngine;
using System.Collections.Generic;

public class PoolManager : Singleton<PoolManager>
{
	private Dictionary<int, Queue<ObjectInstance>> _poolDictioary = new Dictionary<int, Queue<ObjectInstance>>();

	public void CreatePool(GameObject prefab, int poolSize)
	{
		int poolKey = prefab.GetInstanceID();

		if (!_poolDictioary.ContainsKey(poolKey))
		{
            _poolDictioary.Add(poolKey, new Queue<ObjectInstance>());

			GameObject poolHolder = new GameObject(prefab.name + " pool");
			poolHolder.transform.parent = transform;

			for (int i = 0; i < poolSize; i++)
			{
				ObjectInstance newObject = new ObjectInstance(Instantiate(prefab) as GameObject);
                _poolDictioary[poolKey].Enqueue(newObject);
				newObject.SetParent(poolHolder.transform);
			}
		}
	}

	public GameObject ReuseObject(GameObject prefab, Vector3 position, Quaternion rotation)
	{
		int poolKey = prefab.GetInstanceID();

		if (_poolDictioary.ContainsKey(poolKey))
		{
			ObjectInstance objectToReuse = _poolDictioary[poolKey].Dequeue();
            _poolDictioary[poolKey].Enqueue(objectToReuse);

			objectToReuse.Reuse(position, rotation);

            return objectToReuse.GameObject;
		}

        return null;
	}

    public void DespawnAll(GameObject prefab)
    {
        int poolKey = prefab.GetInstanceID();

        if (_poolDictioary.ContainsKey(poolKey))
        {
            Queue<ObjectInstance> pool = _poolDictioary[poolKey];
            foreach (ObjectInstance obj in pool)
            {
                obj.Despawn();
            }
        }
    }

    public class ObjectInstance
	{
        private Transform _transform;

		private bool _hasPoolObjectComponent;
		private PoolObject _poolObject;

        public GameObject GameObject { get; }

        public ObjectInstance(GameObject objectInstance)
		{
			GameObject = objectInstance;
			_transform = GameObject.transform;
			GameObject.SetActive(false);

			if (GameObject.GetComponent<PoolObject>())
			{
				_hasPoolObjectComponent = true;
				_poolObject = GameObject.GetComponent<PoolObject>();
			}
		}

		public void Reuse(Vector3 position, Quaternion rotation)
		{
			GameObject.SetActive(true);
			_transform.position = position;
			_transform.rotation = rotation;

			if (_hasPoolObjectComponent)
			{
				_poolObject.OnObjectReuse();
			}
		}

        public void Despawn()
        {
            GameObject.SetActive(false);
        }

        public void SetParent(Transform parent)
		{
			_transform.parent = parent;
		}
	}
}