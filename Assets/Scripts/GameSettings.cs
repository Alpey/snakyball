﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName ="Game Data/Game Settings")]
public class GameSettings : ScriptableObject
{
    public enum DifficultyLevel
    {
        Easy,
        Normal,
        Hard
    }

    public enum CrystalGenerationMode
    {
        Random,
        Sequence
    }

    [System.Serializable]
    public class DifficultySettings
    {
        public DifficultyLevel DifficultyLevel = DifficultyLevel.Easy;
        public float TileSize = 1f;
        public GameObject TilePrefab;
        public float CameraSize = 5.75f;
        public float BallSpeed = 3f;
        public float TileFieldBoundaries = 2f;
    }

    public List<DifficultySettings> DifficultySettingsList = new List<DifficultySettings>();

    [HideInInspector]
    public DifficultySettings CurrentDifficulty;

    public CrystalGenerationMode CurrentCrystalGenerationMode;
    public int CrystalGenerationStep = 5;
}
