﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Camera _mainCamera;

    [SerializeField]
    private Transform _target;

    [SerializeField]
    private Vector3 _offset;

    [SerializeField]
    private bool _ignoreHorizontal = false;

    [SerializeField]
    private bool _ignoreVertical = false;

    private Transform _tr;

    public bool IsFollowing { get; set; }

    private void Awake()
    {
        _tr = transform;
    }

    private void Start()
    {
        IsFollowing = true;
        SetPosition();
    }

    private void OnEnable()
    {
        GameEventSystem.Events.PlayerFellEvent.AddListener(StopFollowing, this);
        GameEventSystem.Events.LevelRestartEvent.AddListener(StartFollowing, this);
        GameEventSystem.Events.LevelStartWithDifficultyEvent.AddListener(InitWithDifficultySettings, this);
    }

    private void OnDisable()
    {
        GameEventSystem.Events.PlayerFellEvent.RemoveListener(StopFollowing);
        GameEventSystem.Events.LevelRestartEvent.RemoveListener(StartFollowing);
        GameEventSystem.Events.LevelStartWithDifficultyEvent.RemoveListener(InitWithDifficultySettings);
    }

    public void InitWithDifficultySettings(GameSettings.DifficultySettings difficultySettings)
    {
        if (_mainCamera != null)
        {
            _mainCamera.orthographicSize = difficultySettings.CameraSize;
        }
    }

    private void LateUpdate()
    {
        if (IsFollowing)
        {
            SetPosition();
        }
    }

    private void SetPosition()
    {
        if (_target != null)
        {
            var targetPos = new Vector3(!_ignoreHorizontal ? _target.position.x : 0f,
                _target.position.y,
                !_ignoreVertical ? _target.position.z : 0f);

            _tr.position = targetPos + _offset;
        }
    }

    private void StopFollowing()
    {
        IsFollowing = false;
    }

    private void StartFollowing()
    {
        IsFollowing = true;
    }
}
