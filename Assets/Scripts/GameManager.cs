﻿using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    private GameSettings _gameSettings;
    private int _score;
    private bool _inGame = false;

    public const string DIFFICULTY_KEY = "Difficulty";

    public GameSettings GameSettings
    {
        get
        {
            return _gameSettings;
        }
    }

    public bool InGame
    {
        get
        {
            return !UIManager.Instance.IsPopupOpen && _inGame;
        }
    }

    private void Start()
    {
        SetDifficultyLevel((GameSettings.DifficultyLevel)PlayerPrefs.GetInt(DIFFICULTY_KEY, 1));
        GameEventSystem.Events.LevelStartWithDifficultyEvent.Invoke(GameSettings.CurrentDifficulty);
    }

    private void OnEnable()
    {
        GameEventSystem.Events.CrystalCollectedEvent.AddListener(OnScoreUpdate, this);
        GameEventSystem.Events.LevelRestartEvent.AddListener(ResetScore, this);
        GameEventSystem.Events.PlayButtonClickEvent.AddListener(StartGame, this);
    }

    private void OnDisable()
    {
        GameEventSystem.Events.CrystalCollectedEvent.RemoveListener(OnScoreUpdate);
        GameEventSystem.Events.LevelRestartEvent.RemoveListener(ResetScore);
        GameEventSystem.Events.PlayButtonClickEvent.RemoveListener(StartGame);
    }

    private void OnScoreUpdate(int scoreAddition)
    {
        SetScore(_score + scoreAddition);
    }

    private void SetScore(int newScore)
    {
        _score = newScore;
        UIManager.Instance.SetScore(_score);
    }

    private void ResetScore()
    {
        SetScore(0);
    }

    private void StartGame()
    {
        _inGame = true;
    }

    public void SetDifficultyLevel(GameSettings.DifficultyLevel difficultyLevel)
    {
        if (GameSettings.CurrentDifficulty.DifficultyLevel== difficultyLevel)
        {
            return;
        }

        int index = (int)difficultyLevel;

        if (index < GameSettings.DifficultySettingsList.Count)
        {
            GameSettings.CurrentDifficulty = GameSettings.DifficultySettingsList[index];
            PlayerPrefs.SetInt(DIFFICULTY_KEY, index);
            GameEventSystem.Events.LevelStartWithDifficultyEvent.Invoke(GameSettings.CurrentDifficulty);
        }
    }
}
