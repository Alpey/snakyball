﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuPopup : MonoBehaviour
{
    public void OnPlayButton()
    {
        gameObject.SetActive(false);
        GameEventSystem.Events.PlayButtonClickEvent.Invoke();
    }

    public void OnOptionsButton()
    {
        gameObject.SetActive(false);
        UIManager.Instance.OpenOptions();
    }
}
