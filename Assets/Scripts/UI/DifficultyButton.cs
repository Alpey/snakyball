﻿using UnityEngine;
using UnityEngine.UI;

public class DifficultyButton : MonoBehaviour
{
    [SerializeField]
    private Color _selectedButtonColor;

    [SerializeField]
    private Color _deselectedButtonColor;

    [SerializeField]
    private Image _image;

    [SerializeField]
    private GameSettings.DifficultyLevel _difficultyLevel;

    public GameSettings.DifficultyLevel DifficultyLevel { get => _difficultyLevel;}

    public void SetSelected(bool selected)
    {
        if (selected)
        {
            _image.color = _selectedButtonColor;
        }
        else
        {
            _image.color = _deselectedButtonColor;
        }
    }
}
