﻿using TMPro;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    [SerializeField]
    private GameObject _mainMenuPopup;

    [SerializeField]
    private GameObject _optionsPopup;

    [SerializeField]
    private TextMeshProUGUI _scoreValText;

    public bool IsPopupOpen
    {
        get
        {
            return _mainMenuPopup.activeSelf || _optionsPopup.activeSelf;
        }
    }

    public void SetScore(int score)
    {
        _scoreValText.text = score.ToString();
    }

    public void OpenMainMenu()
    {
        _mainMenuPopup.SetActive(true);
    }

    public void OpenOptions()
    {
        _optionsPopup.SetActive(true);
    }
}
