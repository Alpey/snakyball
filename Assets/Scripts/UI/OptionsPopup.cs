﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsPopup : MonoBehaviour
{
    [SerializeField]
    private List<DifficultyButton> _difficultyButtons;

    private bool _updatedDifficulty = false;

    private void OnEnable()
    {
        _updatedDifficulty = false;
    }

    public void OnOptionsBackButton()
    {
        gameObject.SetActive(false);
        if (!GameManager.Instance.InGame)
        {
            UIManager.Instance.OpenMainMenu();
        }
    }

    public void OnDifficultyButton(DifficultyButton button)
    {
        foreach (DifficultyButton difficultyButton in _difficultyButtons)
        {
            difficultyButton.SetSelected(difficultyButton == button);
        }

        if (GameManager.Instance.GameSettings.CurrentDifficulty.DifficultyLevel != button.DifficultyLevel)
        {
            GameManager.Instance.SetDifficultyLevel(button.DifficultyLevel);
            _updatedDifficulty = true;
        }
    }
}
