﻿using System.Collections.Generic;
using UnityEngine;

public class TileField : Singleton<TileField>
{
    [SerializeField]
    private GameObject _tilePrefab;
    [SerializeField]
    private Tile _startTile;
    [SerializeField]
    private int _visibleTilesAmount = 15;
    [SerializeField]
    private float _tileSize = 1f;

    private Transform _lastTileTransform;
    private float _fieldBoundary = 2f;
    private Queue<Tile> _passedTilesQueue = new Queue<Tile>();

    private void Start()
    {
    }

    private void OnEnable()
    {
        GameEventSystem.Events.TilePassedEvent.AddListener(OnTilePassed, this);
        GameEventSystem.Events.LevelRestartEvent.AddListener(Init, this);
        GameEventSystem.Events.LevelStartWithDifficultyEvent.AddListener(InitWithDifficultySettings);
    }

    private void OnDisable()
    {
        GameEventSystem.Events.TilePassedEvent.RemoveListener(OnTilePassed);
        GameEventSystem.Events.LevelRestartEvent.RemoveListener(Init);
        GameEventSystem.Events.LevelStartWithDifficultyEvent.RemoveListener(InitWithDifficultySettings);
    }

    public void InitWithDifficultySettings(GameSettings.DifficultySettings difficultySettings)
    {
        if (_tilePrefab != difficultySettings.TilePrefab)
        {
            PoolManager.Instance.DespawnAll(_tilePrefab);
        }

        _tilePrefab = difficultySettings.TilePrefab;
        _tileSize = difficultySettings.TileSize;
        _fieldBoundary = difficultySettings.TileFieldBoundaries;
        PoolManager.Instance.CreatePool(_tilePrefab, _visibleTilesAmount);
        Init();
    }

    private void Init()
    {
        _passedTilesQueue.Clear();
        _lastTileTransform = _startTile.transform;
        GenerateInitialField();
    }

    private void GenerateInitialField()
    {
        float tileDistance;
        for (int i = 0; i < _visibleTilesAmount; i++)
        {
            tileDistance = i == 0 ? (0.5f * _tileSize + 0.5f) : _tileSize;
            GenerateNextTile(tileDistance, i == 0);
        }
    }

    private GameObject GenerateNextTile(float distBetweenTiles, bool firstTile = false)
    {
        var nextTileOffset = firstTile ? _lastTileTransform.right : (Random.Range(0, 2) == 0 ?
                (_lastTileTransform.position.x > -_fieldBoundary ? _lastTileTransform.forward : _lastTileTransform.right) :
                (_lastTileTransform.position.x < _fieldBoundary ? _lastTileTransform.right : _lastTileTransform.forward)) * distBetweenTiles;
        Vector3 nextTilePos = _lastTileTransform.position + nextTileOffset;

        GameObject nextTile = PoolManager.Instance.ReuseObject(_tilePrefab, nextTilePos, _lastTileTransform.rotation);
        _lastTileTransform = nextTile.transform;

        CrystalManager.Instance.OnTileGenerated(nextTile.transform);

        return nextTile;
    }

    private void OnTilePassed(Tile tile)
    {
        _passedTilesQueue.Enqueue(tile);
        if (_passedTilesQueue.Count > 4)
        {
            Tile passedTile = _passedTilesQueue.Dequeue();
            passedTile.OnDeactivate(() => { GenerateNextTile(_tileSize); });
        }
    }
}
