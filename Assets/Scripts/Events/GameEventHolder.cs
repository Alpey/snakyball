﻿using UnityEngine;

public class GameEventHolder
{
    //Gameplay
    public GameEvent LevelRestartEvent = new GameEvent();
    public GameEvent<GameSettings.DifficultySettings> LevelStartWithDifficultyEvent = new GameEvent<GameSettings.DifficultySettings>();
    public GameEvent<int> CrystalCollectedEvent = new GameEvent<int>();
    public GameEvent PlayerFellEvent = new GameEvent();

    //UI
    public GameEvent PlayButtonClickEvent = new GameEvent();

    //Misc
    public GameEvent<Tile> TilePassedEvent = new GameEvent<Tile>();
}
