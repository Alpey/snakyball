﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

public class GameEventSystem
{
    private GameEventSystem()
    {
        eventHolder = new GameEventHolder();
        listeners = new ListenersDictionnary();
    }

    [SerializeField] private readonly GameEventHolder eventHolder;
    public static GameEventHolder Events
    {
        get { return Instance.eventHolder; }
    }

    private static GameEventSystem _instance;
    public static GameEventSystem Instance
    {
        get
        {
            return _instance ?? (_instance = new GameEventSystem());
        }
    }

    private class ListenersDictionnary : Dictionary<UnityEventBase, List<Remover>>
    {
    };

    private readonly ListenersDictionnary listeners;
    public static bool debug;
    private object registeringContext;

    private delegate bool RemoveCondition(Remover remover);
    private class Remover
    {
        private readonly Delegate eventDelegate;
        private readonly Action remove;
        private readonly object context;

        private Remover(object context)
        {
            this.context = context;
        }

        public Remover(Action removeAction, object context)
            : this(context)
        {
            remove = removeAction;
        }

        public Remover(Delegate @delegate, Action removeAction, object context)
            : this(removeAction, context)
        {
            eventDelegate = @delegate;
        }

        public bool RemoveIfDelegateIs(Delegate d)
        {
            if (eventDelegate != d) return false;
            remove();
            return true;
        }

        public bool RemoveIfContextIs(object contextToRemove)
        {
            if (!ReferenceEquals(contextToRemove, context)) return false;
            remove();
            return true;
        }
    }

    private void UnregisterIf(UnityEventBase @event, RemoveCondition condition)
    {
        List<Remover> eventList;
        if (!listeners.TryGetValue(@event, out eventList)) return;

        for (int index = eventList.Count; --index >= 0;)
        {
            Remover actionRemove = eventList[index];
            if (!condition(actionRemove)) continue;
            eventList.Remove(actionRemove);
            if (eventList.Count == 0)
            {
                listeners.Remove(@event);
            }
        }
    }

    public void Unregister(UnityEventBase @event, Delegate actionTarget)
    {
        UnregisterIf(@event, remove => remove.RemoveIfDelegateIs(actionTarget));
    }

    private void RemoveFromAllEvents(object context)
    {
        if (context == null) throw new NullReferenceException();
        List<UnityEventBase> toRemove = null;
        foreach (var keyValuePair in listeners)
        {
            UnityEventBase evt = keyValuePair.Key;
            List<Remover> removers = keyValuePair.Value;
            for (int index = removers.Count; --index >= 0;)
            {
                Remover remover = removers[index];
                if (!remover.RemoveIfContextIs(context)) continue;

                int lastIndex = removers.Count - 1;
                if (lastIndex > 0)
                {
                    removers[index] = removers[lastIndex];
                    removers.RemoveAt(lastIndex);
                }
                else
                {
                    removers.Clear();
                    if (toRemove == null)
                    {
                        toRemove = new List<UnityEventBase>();
                    }
                    toRemove.Add(evt);
                }
            }
        }

        if (toRemove != null)
        {
            for (int index = 0; index < toRemove.Count; index++)
            {
                listeners.Remove(toRemove[index]);
            }
        }
    }

    public void Register(GameEvent @event, UnityAction action)
    {
        AddTargetDelegate(@event, action, () => @event.DirectRemoveListener(action));
        @event.DirectAddListener(action);

    }

    public void RegisterPayload<T>(GameEvent<T> @event, UnityAction<T> action)
    {
        AddTargetDelegate(@event, action, () => @event.DirectRemoveListener(action));
        @event.DirectAddListener(action);
    }

    public void AddTargetMethod(UnityEventBase @event, Action removeAction)
    {
        Remover ar = new Remover(removeAction, registeringContext);
        AddTarget(@event, ar);
    }

    private void AddTargetDelegate(UnityEventBase @event, Delegate actionTarget, Action removeAction)
    {
        Remover ar = new Remover(actionTarget, removeAction, registeringContext);
        AddTarget(@event, ar);
    }

    private void AddTarget(UnityEventBase @event, Remover ar)
    {
		List<Remover> targetList;
        if (!listeners.TryGetValue(@event, out targetList))
        {
            targetList = new List<Remover>();
            listeners.Add(@event, targetList);
        }
        targetList.Add(ar);
    }

    public class RegisteringContext : IDisposable
    {
        private readonly GameEventSystem system;
        public RegisteringContext(GameEventSystem system, object registeringObject)
        {
            this.system = system;
            if (registeringObject == null)
            {
                throw new NullReferenceException();
            }
            system.registeringContext = registeringObject;
        }

        public static implicit operator GameEventHolder(RegisteringContext reg)
        {
            return reg.system.eventHolder;
        }

        public void Dispose()
        {
            system.registeringContext = null;
        }
    }

    private RegisteringContext RegisterContextInternal(object context)
    {
        return new RegisteringContext(this, context);
    }

    public static RegisteringContext Context(object context)
    {
        return Instance.RegisterContextInternal(context);
    }

    public static void UnregisterFromAllEvents(object context)
    {
        Instance.RemoveFromAllEvents(context);
    }

    public void ResetContext()
    {
        registeringContext = null;
    }
}