﻿using System;
using UnityEngine;
using UnityEngine.Events;

public interface IEmitable
{
    Type PayloadType { get; }
}

[Serializable]
public class GameEvent : UnityEvent, IEmitable
{
    public new void Invoke()
    {
        base.Invoke();
    }


    public Type PayloadType
    {
        get { return typeof(void); }
    }

    public new void AddListener(UnityAction action)
    {
        GameEventSystem.Instance.Register(this, action);
    }

    public void AddListener(UnityAction action, object context)
    {
        using (GameEventSystem.Context(context))
        {
            AddListener(action);
        }
    }
    
    public new void RemoveListener(UnityAction call)
    {
        GameEventSystem.Instance.Unregister(this, call);
    }

    [Obsolete("This function is only meant to be called by the GameEventSystem. Use AddListener instead.")]
    public void DirectAddListener(UnityAction action)
    {
        base.AddListener(action);
    }

    [Obsolete("This function is only meant to be called by the GameEventSystem. Use RemoveListener instead.")]
    public void DirectRemoveListener(UnityAction action)
    {
        base.RemoveListener(action);
    }
}

[Serializable]
public class GameEvent<T> : UnityEvent<T>, IEmitable
{
    public new void Invoke(T p)
    {
        base.Invoke(p);
    }

    public Type PayloadType
    {
        get { return typeof(T); }
    }

    public new void AddListener(UnityAction<T> action)
    {
        GameEventSystem.Instance.RegisterPayload(this, action);
    }

    public void AddListener(UnityAction<T> action, object context)
    {
        using (GameEventSystem.Context(context))
        {
            AddListener(action);
        }
    }

    public new void RemoveListener(UnityAction<T> call)
    {
        GameEventSystem.Instance.Unregister(this, call);
    }

    [Obsolete("This function is only meant to be called by the GameEventSystem. Use AddListener instead.")]
    public void DirectAddListener(UnityAction<T> action)
    {
        base.AddListener(action);
    }

    [Obsolete("This function is only meant to be called by the GameEventSystem. Use Remove instead.")]
    public void DirectRemoveListener(UnityAction<T> action)
    {
        base.RemoveListener(action);
    }
}
