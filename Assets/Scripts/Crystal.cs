﻿using UnityEngine;

public class Crystal : PoolObject
{
    [SerializeField]
    private int _givenPoints = 1;

    [SerializeField]
    private MeshRenderer _meshRenderer;

    [SerializeField]
    private GameObject _collectedEffect;

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "PlayerBall")
        {
            OnCollected();
        }
    }

    private void OnCollected()
    {
        _meshRenderer.enabled = false;
        _collectedEffect.SetActive(true);
        GameEventSystem.Events.CrystalCollectedEvent.Invoke(_givenPoints);
    }

    public override void OnObjectReuse()
    {
        _meshRenderer.enabled = true;
        _collectedEffect.SetActive(false);
    }
}
