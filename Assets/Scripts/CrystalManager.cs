﻿using System.Collections.Generic;
using UnityEngine;

public class CrystalManager : Singleton<CrystalManager>
{
    [SerializeField]
    private GameObject _crystalPrefab;

    private List<Transform> _pastGeneratedTiles = new List<Transform>();
    private int _currentStep = 0;
    private int _generatedTilesLimit;

    private void Awake()
    {
        PoolManager.Instance.CreatePool(_crystalPrefab, 20);
    }

    private void OnEnable()
    {
        GameEventSystem.Events.LevelRestartEvent.AddListener(OnLevelRestart, this);
    }

    private void OnDisable()
    {
        GameEventSystem.Events.LevelRestartEvent.RemoveListener(OnLevelRestart);
    }

    private void OnLevelRestart()
    {
        PoolManager.Instance.DespawnAll(_crystalPrefab);
    }

    public void OnTileGenerated(Transform tile)
    {
        _generatedTilesLimit = GameManager.Instance.GameSettings.CrystalGenerationStep;
        
        if (_pastGeneratedTiles.Count >= _generatedTilesLimit)
        {
            GenerateCrystal();
            if (++_currentStep >= _generatedTilesLimit)
            {
                _currentStep = 0;
            }
            _pastGeneratedTiles.Clear();
        }

        _pastGeneratedTiles.Add(tile);
    }

    private void GenerateCrystal()
    {
        int tileIndex = 0;

        switch (GameManager.Instance.GameSettings.CurrentCrystalGenerationMode)
        {
            case GameSettings.CrystalGenerationMode.Random:
                tileIndex = Random.Range(0, _generatedTilesLimit);
                break;
            case GameSettings.CrystalGenerationMode.Sequence:
                tileIndex = _currentStep;
                break;
        }
        PoolManager.Instance.ReuseObject(_crystalPrefab, _pastGeneratedTiles[tileIndex].position, Quaternion.identity);
    }
}
