﻿using System;
using System.Collections;
using UnityEngine;

public class Tile : PoolObject
{
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private string _appearAnimParamName = "Appear";
    [SerializeField]
    private bool _alwaysActive;

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.name == "PlayerBall")
        {
            GameEventSystem.Events.TilePassedEvent.Invoke(this);
        }
    }

    public void OnDeactivate(Action callback)
    {
        if (_alwaysActive)
        {
            return;
        }

        StartCoroutine(DeactivationCoroutine(callback));
    }

    private IEnumerator DeactivationCoroutine(Action callback)
    {
        _animator.SetBool(_appearAnimParamName, false);
        yield return new WaitForSeconds(1f);

        if (callback != null)
            callback();
    }

    public override void OnObjectReuse()
    {
        _animator.SetBool(_appearAnimParamName, true);
    }
}
