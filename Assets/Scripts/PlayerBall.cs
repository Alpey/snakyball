﻿using UnityEngine;

public class PlayerBall : MonoBehaviour
{
    public enum MovementDirection
    {
        Left,
        Right
    }

    [SerializeField]
    private float _fallHeight = -15f;

    [SerializeField]
    private Transform _spawnPoint;

    private Transform _tr;
    private Rigidbody _rb;
    private MovementDirection _currentDirection;
    private float _movementSpeed = 1f;

    public Vector3 MovementVector
    {
        get
        {
            return _currentDirection == MovementDirection.Left ? _tr.forward : _tr.right;
        }
    }

    public bool IsFalling
    {
        get
        {
            return _rb.velocity.y < -1f;
        }
    }

    public bool IsMoving { get; private set; }
    public bool IsGrounded { get; private set; }

    private void Awake()
    {
        _tr = transform;
        _rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        Respawn();
    }

    private void OnEnable()
	{
		GameEventSystem.Events.LevelRestartEvent.AddListener(Respawn, this);
		GameEventSystem.Events.LevelStartWithDifficultyEvent.AddListener(InitWithDifficultySettings, this);
    }

	private void OnDisable()
	{
		GameEventSystem.Events.LevelRestartEvent.RemoveListener(Respawn);
        GameEventSystem.Events.LevelStartWithDifficultyEvent.RemoveListener(InitWithDifficultySettings);
    }

    public void InitWithDifficultySettings(GameSettings.DifficultySettings difficultySettings)
    {
        _movementSpeed = difficultySettings.BallSpeed;
    }

	private void Update()
    {
        var interactionDone = GameManager.Instance.InGame &&
            (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space));

        if (!IsMoving)
        {
            if (interactionDone)
            {
                IsMoving = true;
            }
            return;
        }

        if (!IsFalling)
        {
            if (interactionDone)
            {
                ToggleDirection();
            }
        }
        else
        {
            if (IsGrounded)
            {
                IsGrounded = false;
				GameEventSystem.Events.PlayerFellEvent.Invoke();
            }
            if (_tr.position.y < _fallHeight)
            {
                Respawn();
				GameEventSystem.Events.LevelRestartEvent.Invoke();
			}
        }
    }

    private void FixedUpdate()
    {
        if (!IsMoving)
        {
            return;
        }

        Vector3 movement = _movementSpeed * Time.deltaTime * MovementVector;
        _rb.MovePosition(_tr.position + movement);
    }

    private void ToggleDirection()
    {
        ChangeDirection(_currentDirection == MovementDirection.Left ?
            MovementDirection.Right : MovementDirection.Left);
    }

    private void ChangeDirection(MovementDirection newDirection)
    {
        if (newDirection == _currentDirection)
            return;

        _currentDirection = newDirection;
        _rb.velocity = Vector3.zero;
    }

    private void Respawn()
    {
        _tr.position = _spawnPoint.position;
        _currentDirection = MovementDirection.Right;
        _rb.velocity = Vector3.zero;
        IsGrounded = true;
        IsMoving = false;
    }
}
